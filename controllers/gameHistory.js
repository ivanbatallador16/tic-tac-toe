// Dependencies and Modules
const GameHistory = require("../models/GameHistory");


module.exports.gameHistory = async (req, res) => {

	try {
		    let newGame = new GameHistory({
			      playerXName: req.body.playerXName,
			      playerOName: req.body.playerOName,
			      playerXWin: req.body.playerXWin,
			      playerOWin: req.body.playerOWin,
			      numberOfDraw: req.body.numberOfDraw
		    });
		    let savedGame = await newGame.save();
		    console.log(savedGame)
		    res.status(201).json(savedGame);
	}
	catch (error) {
	    console.error(error);
	    res.status(500).json({ error: 'An error occurred during saving.' });
	  }
}


module.exports.gameData = async (req, res) => {
	  try {
	    const gameData = await GameHistory.find();
	    res.json(gameData);
	  } catch (error) {
	    console.error('Error fetching game data:', error);
	    res.status(500).json({ error: 'Internal Server Error' });
	  }
};