// Dependencies and Modules
const express = require("express");
const gameHistoryController = require("../controllers/gameHistory");


// Routing Component
const router = express.Router();

//Routes

// Route for Game History
router.post('/gameHistory', gameHistoryController.gameHistory);

router.get ('/', gameHistoryController.gameData);


// Export Route System
module.exports = router;