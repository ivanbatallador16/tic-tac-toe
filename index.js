// Import dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Import Routes
const gameHistoryRoutes = require("./routes/gameHistory");


// Server setup
const app = express();
const port = 4000;


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


//Connecting to MongoDB Locally
mongoose.connect("mongodb+srv://ivanbatallador:Batallador-16@b320-cluster.o7ltg8e.mongodb.net/tictactoeAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
  	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database!"));

// Backend Routes
app.use("/gameHistory", gameHistoryRoutes);



// Server start
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;