// Modules and Dependencies
const mongoose = require("mongoose");



// Schema/Blueprint
const gameHistorySchema = new mongoose.Schema({

	playerXName: {
			type: String,
			required: [true, "Player X's Name is required"]
		},
	playerOName: {
			type: String,
			required: [true, "Player O's Name is required"]
		},
	timeAndDate: {
			type: Date,
			default: Date.now
		},
	playerXWin: {
		type: Number,
		default: 0
		},
	playerOWin: {
		type: Number,
		default: 0
		},
	numberOfDraw: {
		type: Number,
		default: 0
	}

});

// Model
module.exports = mongoose.model("GameHistory", gameHistorySchema);